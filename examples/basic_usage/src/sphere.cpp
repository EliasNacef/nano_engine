#pragma once

#include <sphere.hpp>

Sphere::Sphere(std::weak_ptr<engine::World> world, const std::string& name, float radius) :
	Base(world, name),
	m_radius(radius)
{
	m_collider = AddComponent<nano_engine::components::SphereCollider>(m_radius);
	m_rigidBody = AddComponent<nano_engine::components::RigidBody>(m_collider.Get(), 1.0f, GetPosition());
	m_replication = AddComponent<nano_engine::components::ReplicationComponent>(this);
}

void Sphere::SetPosition(float x, float y, float z)
{
	Base::SetPosition(x, y, z);
	m_rigidBody.Get().SetPosition(x, y, z);
}

void Sphere::Write(serialization::OutputMemoryStream& stream) const
{
	stream.Write(m_radius);
}

void Sphere::Read(serialization::InputMemoryStream& stream)
{
	m_radius = stream.Read<float>();
	//TODO : update collider
}