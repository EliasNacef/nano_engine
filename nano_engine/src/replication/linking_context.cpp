#include <map>

#include <nano_engine/common.hpp>
#include <nano_engine/replication/linking_context.hpp>

namespace nano_engine::replication
{
	engine::ObjectID_t LinkingContext::GetObjectID(engine::Entity* entity) const
	{
		if (auto it = m_entitiesToId.find(entity); it != m_entitiesToId.end())
		{
			return it->second;
		}
		return 0;
	}

	engine::Entity* LinkingContext::GetEntity(engine::ObjectID_t objIDd) const
	{
		if (auto it = m_idToEntities.find(objIDd); it != m_idToEntities.end())
		{
			return it->second;
		}
		return nullptr;
	}

	engine::ObjectID_t LinkingContext::AddEntity(engine::Entity* entity)
	{
		if (entity == nullptr) return 0;
		auto objID = ++m_nextObjectID;
		m_idToEntities.insert(std::make_pair(objID, entity));
		m_entitiesToId.insert(std::make_pair(entity, objID));
		return objID;
	}
	void LinkingContext::RemoveEntity(engine::Entity* entity)
	{
		if (entity == nullptr) return;

		if (auto it = m_entitiesToId.find(entity); it != m_entitiesToId.end())
		{
			auto objID = it->second;
			m_entitiesToId.erase(it);
			m_idToEntities.erase(objID);
		}

	}
	void LinkingContext::RemoveEntity(engine::ObjectID_t objID)
	{
		if (objID == 0) return;
		if (auto it = m_idToEntities.find(objID); it != m_idToEntities.end())
		{
			auto entity = it->second;
			m_idToEntities.erase(it);
			m_entitiesToId.erase(entity);

		}
	}
}